#[allow(dead_code)]
#[derive(Debug)]

enum Color {
  Red,
  Blue,
  Green,
  RGB(u32, u32, u32),
  HSV(u32, u32, u32),
  HSL(u32, u32, u32),
  CMY(u32, u32, u32),
  CMYK(u32, u32, u32, u32)
}

fn main() {
  let color = Color::RGB(122, 17, 40);

  println!("What color is it?");

  match color {
    Color::Red => println!("The color is Red!"),
    Color::Blue => println!("The color is Blue!"),
    Color::Green => println!("The color is Green!"),
    Color::RGB(r, g, b) => println!("R:{}, G:{}, B:{}", r, g, b),
    Color::HSV(r, g, b) => println!("H:{}, S:{}, V:{}", r, g, b),
    Color::HSL(r, g, b) => println!("H:{}, S:{}, L:{}", r, g, b),
    Color::CMY(r, g, b) => println!("C:{}, M:{}, Y:{}", r, g, b),
    Color::CMYK(r, g, b, k) => println!("C:{}, M:{}, Y:{}, K:{}", r, g, b, k),
  }
}
