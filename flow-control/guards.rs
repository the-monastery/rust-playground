fn main() {
  let pair = (2, -1);

  println!("Tell me about {:?}", pair);

  match pair {
    (x, y) if x == y => println!("These are twines."),
    (x, y) if x + y == 0 => println!("Antimatter -- KABOOM!"),
    (x, _) if x % 2 == 1 => println!("The first one is odd"),
    _ => println!("No correlation..."),
  }
}
