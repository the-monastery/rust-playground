fn main() {
  let mut optional = Some(0);

  while let Some(i) = optional {
    if i > 9 {
      println!("Greater than 9, quit!");
      optional = None;
    } else {
      println!("`i` is `{:?}`. Try again.", i);
      optional = Some(i + 1);
    }
  }

  println!("This more complicated way achieves the same results.");

  //This shows a more complicated structure
  //that achieves the same goal... 1 is better
  let mut optional2 = Some(0);
  
  loop {
    match optional2 {
      Some(i) => {
        if i > 9 {
          println!("Greater than 9, quit!");
          optional2 = None;
        } else {
          println!("`i` is `{:?}`. Try again.", i);
          optional2 = Some(i + 1);
        }
      },
      _ => { break; }
    }
  }
}
