fn main() {
  let mut long_lived_binding = 1;
  println!("outer long: {}", long_lived_binding);
  
  //you can just create random scoped blocks like this.
  {
    let short_lived_binding = 2;
    println!("inner short: {}", short_lived_binding);

    long_lived_binding = 32;
    println!("inner long: {}", long_lived_binding);
  }

  //short_lived_binding doesn't exist outside of the scoped block
  //println!("output short: {}", short_lived_binding);
  
  println!("outer long: {}", long_lived_binding);
  
  //This binding also *shadows* the previous binding
  let long_lived_binding = 'a';
  println!("outer long: {}", long_lived_binding);
}
