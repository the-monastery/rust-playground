fn main() {
  let immutable_binding = 1;
  let mut mutable_binding = 1;

  println!("Before mutation: {}", mutable_binding);
  mutable_binding += 1;

  println!("After mutation: {}", mutable_binding);

  let dealing_with_immutables  = immutable_binding + 1;
  println!("Dealing with immutables: {}", dealing_with_immutables);
  
  //Compiler throws a detailed diagnostic... comment out this line to compile.
  //immutable_binding += 1;
}
